#include "Zener_game_result_storage.h"
#include <Arduino.h>


#define LINE_LEN 11
const char format[] PROGMEM = "%03u;%03u;%u\r\n";

SdFat sd;

void Zener_result_storage::init(uint8_t cs) {
    while (!sd.begin(cs, SD_SCK_MHZ(50))) { }
}

void Zener_result_storage::store_result(char const * file, GameState * st) {
    auto f = sd.open(file, FILE_WRITE);
    if(!f) return;
    char buf[LINE_LEN + 1];
    sprintf_P(buf, format, st->guessed_right, st->steps, st->result());
    f.print(buf);
    f.close();
}


uint16_t Zener_result_storage::count_results(char const * file) {
    auto f = sd.open(file, FILE_READ);
    if (!f) return 0;
    uint32_t res = f.size() / LINE_LEN;
    f.close();
    return res;
}

uint8_t Zener_result_storage::read_results(char const * file, Zener_game_result* data, uint16_t offset, uint8_t count) {
    File f = sd.open(file, FILE_READ);
    if (!f) {
        return 0;
    }
    if (f.seek(offset * LINE_LEN)) {
        uint8_t i = 0;
        char buf[LINE_LEN + 1];
        while(f.position() + LINE_LEN <= f.size() && i < count) {
            i++;
            f.readBytes(buf, LINE_LEN);
            buf[LINE_LEN] = 0;
            sscanf_P(buf, format, &data[i].guessed_right, &data[i].steps, &data[i].result);
        }
        f.close();
        return i;
    }
    f.close();
    return 0;
}


Zener_result_lister::Zener_result_lister(char const * player_) {
    strcpy(player, player_);
    strcat(player, ".csv");
    count_results = Zener_result_storage::count_results(player);
}


// Строки закодированы в koi8-r
const char c0[] PROGMEM = "\xce\xc5\xd4"; // нет
const char c1[] PROGMEM = "\xd3\xcc\xc1\xc2\xd9\xc5"; // слабые
const char c2[] PROGMEM = "\xd7\xd9\xc4\xc1\xc0\xdd\xc9\xc5\xd3\xd1"; // выдающиеся
const char c3[] PROGMEM = "\xd0\xd2\xc5\xc4\xc5\xcc\xd8\xce\xd9\xc5"; // предельные
const char c4[] PROGMEM = "\xc1\xc2\xd3\xcf\xcc\xc0\xd4\xce\xd9\xc5"; // абсолютные
const char *const caps_table[] PROGMEM = {c0, c1, c2, c3, c4};


void Zener_result_lister::show_page() {
    result_table_page.show();
    result_btn_prev._setVis(page != 0);
    result_btn_next._setVis(count_results > (lines_per_page * (page + 1)));
    Zener_game_result data[lines_per_page];

    //auto page = count_results > lines_per_page ? count_results - lines_per_page : 0;
    uint16_t line_offset_end = count_results - (lines_per_page * page);
    uint16_t line_offset = line_offset_end > lines_per_page ? line_offset_end - lines_per_page : 0;
    auto cnt = Zener_result_storage::read_results(player, data, line_offset, line_offset_end - line_offset);

    //Serial2.print("show0 2"); Serial2.println(cnt); Serial2.flush();

    for (auto i = 0; i < lines_per_page; i++) {
        if (i < cnt) {
            /*Serial2.print(data[i].guessed_right); Serial2.print(' '); 
            Serial2.print(data[i].steps); Serial2.print(' ');
            Serial2.println(data[i].result); 
            Serial2.flush();*/
            char buf[35];
            sprintf_P(buf, PSTR(" %3u   %3u/%-3u    %S"), 
              count_results - (lines_per_page * page) - i, 
              data[cnt-i].guessed_right, 
              data[cnt-i].steps, 
              (char *)pgm_read_word(&(caps_table[data[cnt-i].result]))
            ); 
            //Serial2.println(buf); Serial2.flush();
            result_lines[i]->setText(buf);
        } else {
            result_lines[i]->setText("");
        }
    }
}

void Zener_result_lister::next() {
    page++;
    show_page();
}

void Zener_result_lister::prev() {
    page--;
    show_page();
}

