#include <Arduino.h>
#include <Nextion.h>
#include "Zener_game.h"
#include "Zener_game_result_storage.h"
#include "timer.h"
#include <avr/sleep.h>
#include <EEPROM.h>
#include "psycho_age.h"

#define DEBUG

void send_bat();
auto battery = Timer();
auto clean_card = Timer();

#define card_assoc(x) (x)
#define card_back 5

unsigned short saved_card_count = 25;

GameState * game;

NexPage game_select_page = NexPage(0, 0, "game_select");
auto battery_indicator = NexProgressBar(0, 2, "jBat");
auto btn_select_game1 = NexButton(game_select_page.getObjPid(), 3, "b0");
auto btn_select_game2 = NexButton(game_select_page.getObjPid(), 4, "b1");


auto index_page = NexPage(1, 0, "index");
auto btn_start = NexButton(1, 2, "bStart");
auto btn_results = NexButton(1, 4, "bResults");
auto round_count = NexObject(1, 3, "round_count");

auto game_page = NexPage(2, 0, "game");
auto c1 = NexButton(2, 4, "b0");
auto c2 = NexButton(2, 5, "b1");
auto c3 = NexButton(2, 6, "b2");
auto c4 = NexButton(2, 7, "b3");
auto c5 = NexButton(2, 8, "b4");
auto c_main = NexPicture(2, 2, "p5");
auto progress = NexProgressBar(2, 3, "j0");
auto card_num = NexText(2, 9, "t_card_num");
auto guess_right = NexText(2, 10, "t_guess_right");

auto results_page = NexPage(3, 0, "results");
auto btn_restart = NexButton(results_page.getObjPid(), 2, "bRestart");
auto success_count = NexText(results_page.getObjPid(), 3, "t2");
auto expected_success_count = NexText(results_page.getObjPid(), 4, "t3");
auto result_score = NexPicture(results_page.getObjPid(), 5, "p_score");

NexPage result_table_page = NexPage(8, 0, "results_table");
NexButton result_btn_restart = NexButton(result_table_page.getObjPid(), 8, "bRestart");
NexButton result_btn_prev = NexButton(result_table_page.getObjPid(), 9, "bPrev");
NexButton result_btn_next = NexButton(result_table_page.getObjPid(), 10, "bNext");
NexText result_t1 = NexText(result_table_page.getObjPid(), 2, "t0");
NexText result_t2 = NexText(result_table_page.getObjPid(), 3, "t1");
NexText result_t3 = NexText(result_table_page.getObjPid(), 4, "t2");
NexText result_t4 = NexText(result_table_page.getObjPid(), 5, "t3");
NexText result_t5 = NexText(result_table_page.getObjPid(), 6, "t4");
NexText result_t6 = NexText(result_table_page.getObjPid(), 7, "t5");
NexText *result_lines[] = {&result_t1,&result_t2,&result_t3,&result_t4,&result_t5,&result_t6};


NexTouch *nex_listen_list[] = 
{
    &c1, &c2, &c3, &c4, &c5, &btn_start, &btn_restart, &game_select_page, &btn_select_game1, &btn_select_game2,
    &result_btn_restart, &result_btn_prev, &result_btn_next, &btn_results,
    psycho_age_list, NULL
};

NexButton * cards[] = {&c1, &c2, &c3, &c4, &c5};

Zener_result_lister * result_lister;

void update_progress() {
  progress.setValue(100 * (unsigned int)game->cur_step / game->steps);
  char buf[4];
  card_num.setText(utoa(game->cur_step, buf, 10));
  guess_right.setText(utoa(game->guessed_right, buf, 10));
};

void btn_start_pop(void *ptr) {
  uint32_t count;  
  round_count._getVar("val", &count);
  count = constrain(count, 5, 250);
  EEPROM.update(0, count);
  saved_card_count = count;
  game_page.show();
  free(game);
  game = new GameState(saved_card_count);
  update_progress();
}

void btn_select_game_pop(void *ptr) {
  auto game = *(unsigned char*)ptr;
  if (game == 0) {
    index_page.show();
  } else {
    psycho_age_enter();
  }
}

void btn_restart_pop(void *ptr) {
  game_select_page.show();
  send_bat();
}

void card_pop(void *ptr) {
  if (clean_card.running) return;
  clean_card.start(800);
  auto card = (unsigned char*)ptr;
  c_main.setPic(card_assoc(game->cur_card));
  for (unsigned char i = 0; i < 5; i++) {
    if (*card != i) cards[i]->_setVar("pic", card_back);
  }
  game->input(*card);
}

void result_btn_next_pop(void *ptr) {
  result_lister->next();
}

void result_btn_prev_pop(void *ptr) {
  result_lister->prev();
}

void show_result_table() {
  free(result_lister);
  result_lister = new Zener_result_lister("0");
  result_lister->show_page(0);
}
void show_result_table(void * ptr) {
  show_result_table();
}

unsigned char numbers[] = {0,1,2,3,4};

void setup_cards() {
  for (unsigned char i = 0; i < 5; i++) {
    cards[i]->attachPop(card_pop, &numbers[i]);
  }
}

void send_bat() {
  auto res = analogRead(A9);
  battery_indicator.setValue((constrain(res, 675, 858) - 675) * 100 / 183);
  battery.start(1000UL * 60 * 5);
}


void setup() {
  while(!nexInit()) {}
  Zener_result_storage::init(10);

  auto saved = EEPROM.read(0);
  if (saved >= 5 && saved <= 250) {
    saved_card_count = saved;
  }
  round_count._setVar("val", saved_card_count);

  btn_start.attachPop(btn_start_pop);
  btn_restart.attachPop(btn_restart_pop);
  btn_select_game1.attachPop(btn_select_game_pop, &numbers[0]);
  btn_select_game2.attachPop(btn_select_game_pop, &numbers[1]);
  btn_results.attachPop(show_result_table);

  result_btn_next.attachPop(result_btn_next_pop);
  result_btn_prev.attachPop(result_btn_prev_pop);
  result_btn_restart.attachPop(btn_restart_pop);

  setup_cards();
  battery.start(0);
  pinMode(A3, INPUT);
  set_sleep_mode(SLEEP_MODE_IDLE);
  sleep_enable();

  //show_result_table();
}

void loop() {
  nexLoop(nex_listen_list);
  if (clean_card.done()) {
    if (game->is_over()) {
      results_page.show();
      char buf[8];
      sprintf(buf, "%d/%d", game->guessed_right, game->steps);
      success_count.setText(buf);
      expected_success_count.setText(itoa(game->expected_right_guesses(), buf, 10));
      auto result = game->result();
      result_score.setPic(result+9);
      Zener_result_storage::store_result("0.csv", game);
    } else {
      c_main.setPic(card_back);
      for (unsigned char i = 0; i < 5; i++) {
        cards[i]->_setVar("pic", card_assoc(i));
      }
      update_progress();
    }
  }
  if (battery.done()) {
    send_bat();
  }
  sleep_cpu();
}