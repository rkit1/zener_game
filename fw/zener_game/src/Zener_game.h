#pragma once

#ifndef NUM_CARDS
#define NUM_CARDS 5
#endif

class GameState{
public:
  unsigned char steps;
  unsigned char cur_card;
  unsigned char cur_step;
  unsigned char guessed_right;
  GameState(unsigned char steps);
  void random_card();
  void input(unsigned char card);
  bool is_over();
  unsigned char expected_right_guesses();
  unsigned char result();
};

