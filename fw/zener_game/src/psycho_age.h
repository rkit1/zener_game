#ifndef PSYCHO_AGE_H
#define PSYCHO_AGE_H
#include <Nextion.h>

#define psycho_age_list &psycho_result_btn_next, &psycho_game_btn_start, &psycho_game_ch0, &psycho_game_ch1, \
&psycho_game_ch2, &psycho_game_ch3, &psycho_game_ch4

extern NexPage game_select_page;
extern NexPage psycho_index_page;
extern NexButton psycho_game_btn_start;

extern NexPage psycho_game_page;
extern NexPicture psycho_game_bg;
extern NexTouch psycho_game_ch0;
extern NexTouch psycho_game_ch1;
extern NexTouch psycho_game_ch2;
extern NexTouch psycho_game_ch3;
extern NexTouch psycho_game_ch4;

extern NexPage psycho_result_page;
extern NexText psycho_result_score;
extern NexPicture psycho_result_bg;
extern NexButton psycho_result_btn_next;

void psycho_age_setup();
void psycho_age_enter();

#endif