#include "Zener_game.h"
#include <Arduino.h>

GameState::GameState(unsigned char _steps) {
    randomSeed(analogRead(0) + micros());
    random_card();
    guessed_right = 0;
    cur_step = 0;
    this->steps = _steps;
}

void GameState::input(unsigned char card) {
    guessed_right += cur_card == card;
    random_card();
    cur_step++;
}

void GameState::random_card() {
    cur_card = random(0, NUM_CARDS);
}

bool GameState::is_over() {
    return cur_step >= steps;
}

unsigned char GameState::expected_right_guesses() {
    return steps / NUM_CARDS;
}

unsigned char GameState::result() {
    auto result = min(guessed_right / max(expected_right_guesses(), 1), 5);
    if (result > 0) result--;
    return result;
}