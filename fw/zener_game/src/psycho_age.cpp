#include "psycho_age.h"
#include "timer.h"
#include <Arduino.h>

void psycho_result_btn_next_pop(void *ptr);
void psycho_btn_start_pop(void *ptr);
void psych_game_ch_pop(void *ptr);

NexPage psycho_index_page = NexPage(5, 0, "game2_index");
NexButton psycho_game_btn_start = NexButton(psycho_index_page.getObjPid(), 2, "bStart");

NexPage psycho_game_page = NexPage(6, 0, "game2");
NexPicture psycho_game_bg = NexPicture(psycho_game_page.getObjPid(), 1, "g2_bg");
NexTouch psycho_game_ch0 = NexTouch(psycho_game_page.getObjPid(), 2, "m0");
NexTouch psycho_game_ch1 = NexTouch(psycho_game_page.getObjPid(), 3, "m1");
NexTouch psycho_game_ch2 = NexTouch(psycho_game_page.getObjPid(), 4, "m2");
NexTouch psycho_game_ch3 = NexTouch(psycho_game_page.getObjPid(), 5, "m3");
NexTouch psycho_game_ch4 = NexTouch(psycho_game_page.getObjPid(), 6, "m4");
NexTouch * psycho_game_chs[] = {&psycho_game_ch0, &psycho_game_ch1, &psycho_game_ch2, &psycho_game_ch3, &psycho_game_ch4};

NexPage psycho_result_page = NexPage(7, 0, "game2_result");
NexText psycho_result_score = NexText(psycho_result_page.getObjPid(), 2, "g2_score");
NexPicture psycho_result_bg = NexPicture(psycho_result_page.getObjPid(), 1, "g2_result_bg");
NexButton psycho_result_btn_next = NexButton(psycho_result_page.getObjPid(), 3, "g2_res_next");

unsigned char g2_game_score = 0;
unsigned char g2_game_page_pos  = 0;
unsigned char g2_results_page_pos = 0;

Timer page_delay;

void dump_game() {
    psycho_game_bg.setPic(15 + g2_game_page_pos);
}

unsigned char scores[] = {0, 2, 6, 8, 4};
void psycho_age_setup() {
    psycho_result_btn_next.attachPop(psycho_result_btn_next_pop);
    psycho_game_btn_start.attachPop(psycho_btn_start_pop);
    for (unsigned char i = 0; i < 5; i++) {
        psycho_game_chs[i]->attachPop(psych_game_ch_pop, &scores[i]);
    }
};

void psycho_age_enter() {
    static bool init = false;
    if (!init) {
        psycho_age_setup();
        init = true;
    }
    psycho_index_page.show();
}

void psych_game_ch_pop(void *ptr) {
    if (!page_delay.done()) return;
    page_delay.start(500);
    g2_game_score += *(unsigned char*)ptr;
    if (g2_game_page_pos++ < 9) {
        dump_game();
    } else {
        psycho_result_page.show();
        char buf[4];
        psycho_result_score.setText(utoa(g2_game_score, buf, 10));
    }
}

void psycho_result_btn_next_pop(void *ptr) {
    if (!page_delay.done()) return;
    page_delay.start(500);
    if (g2_results_page_pos++ < 2) {
        psycho_result_bg.setPic(25 + g2_results_page_pos);
        psycho_result_score._refresh();
        psycho_result_btn_next._refresh();
    } else {
        game_select_page.show();
    }
}

void psycho_btn_start_pop(void *ptr) {
    page_delay.start(500);
    g2_game_page_pos = 0;
    g2_game_score = 0;
    g2_results_page_pos = 0;
    psycho_game_page.show();
    dump_game();
}