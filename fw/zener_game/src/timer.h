#pragma once

class Timer {
    public:
    unsigned long started_at;
    unsigned long interval;
    bool running = false;
    Timer(){}
    void start(unsigned long _interval);
    bool done();
};