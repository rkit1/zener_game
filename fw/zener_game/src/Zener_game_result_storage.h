#pragma once
#include "Zener_game.h"
#include <Arduino.h>
#include <Nextion.h>
#include <SdFat.h>

extern NexPage result_table_page;
extern NexButton result_btn_restart;
extern NexButton result_btn_prev;
extern NexButton result_btn_next;
extern NexText result_t1;
extern NexText result_t2;
extern NexText result_t3;
extern NexText result_t4;
extern NexText result_t5;
extern NexText result_t6;
extern NexText *result_lines[];

extern SdFat sd;


struct Zener_game_result {
    uint8_t guessed_right;
    uint8_t steps;
    uint8_t result;
};

class Zener_result_storage{
    public:
    static void init (uint8_t cs);
    static void store_result(char const * player, GameState * st);
    static uint16_t count_results(char const * player);
    static uint8_t read_results(char const * player, Zener_game_result* data, uint16_t offset = 0, uint8_t count = 6);
};


class Zener_result_lister {
    char player[9];
    uint16_t count_results;
    uint8_t page = 0;
    static constexpr uint8_t lines_per_page = 6;
    public:

    void next();
    void prev();
    inline void show_page(uint8_t page) { this->page = page; show_page(); }
    void show_page();
    Zener_result_lister(char const * player_);
};