/**
 * @file NexObject.cpp
 *
 * The implementation of class NexObject. 
 *
 * @author  Wu Pengfei (email:<pengfei.wu@itead.cc>)
 * @date    2015/8/13
 * @copyright 
 * Copyright (C) 2014-2015 ITEAD Intelligent Systems Co., Ltd. \n
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 */
#include "NexObject.h"
#include "NexHardware.h"

NexObject::NexObject(uint8_t pid, uint8_t cid, const char *name)
{
    this->__pid = pid;
    this->__cid = cid;
    this->__name = name;
}

uint8_t NexObject::getObjPid(void)
{
    return __pid;
}

uint8_t NexObject::getObjCid(void)
{
    return __cid;
}

const char* NexObject::getObjName(void)
{
    return __name;
}

bool NexObject::_setVar(char const * var, char const * val) {
    auto name = getObjName();
    char* buf = (char*)malloc(strlen(var) + strlen(val) + strlen(name) + 3);
    sprintf_P(buf, PSTR("%s.%s=%s"), name, var, val);
    sendCommand(buf);
    free(buf);
    return recvRetCommandFinished();
}

bool NexObject::_setVar(char const * var, uint32_t val) {
    char buf[11];
    utoa(val, buf, 10);
    return _setVar(var, buf);
}

uint16_t NexObject::_getVar(char const * var, char * buffer, uint16_t len) {
    auto name = getObjName();
    char* buf = (char*)malloc(strlen(var) + strlen(name) + 6);
    sprintf_P(buf, PSTR("get %s.%s"), name, var);
    sendCommand(buf);
    free(buf);
    return recvRetString(buffer,len);
}

bool NexObject::_getVar(char const * var, uint32_t * number) {
    auto name = getObjName();
    char* buf = (char*)malloc(strlen(var) + strlen(name) + 6);
    sprintf_P(buf, PSTR("get %s.%s"), name, var);
    sendCommand(buf);
    free(buf);
    return recvRetNumber(number);
}

bool NexObject::_refresh() {
    auto name = getObjName();
    char buf[strlen(name) + 5];
    sprintf_P(buf, PSTR("ref %s"), name);
    sendCommand(buf);
    return recvRetCommandFinished();
}

bool NexObject::_setVis(bool val) {
    auto name = getObjName();
    char buf[strlen(name) + 7];
    sprintf_P(buf, PSTR("vis %s,%d"), name, val > 0);
    sendCommand(buf);
    return recvRetCommandFinished();
}

void NexObject::printObjInfo(void)
{
    dbSerialPrint("[");
    dbSerialPrint((uint32_t)this);
    dbSerialPrint(":");
    dbSerialPrint(__pid);
    dbSerialPrint(",");
    dbSerialPrint(__cid);
    dbSerialPrint(",");
    if (__name)
    {
        dbSerialPrint(__name);
    }
    else
    {
        dbSerialPrint("(null)");
    }
    dbSerialPrintln("]");
}

